output "vip_address" {
  value = var.loadbalancer_options.vip_address
}
output "proxy_port" {
  value = var.loadbalancer_options.proxy_port
}