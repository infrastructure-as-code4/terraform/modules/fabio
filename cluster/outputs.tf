output "virtual_machine_hostnames" {
  value = module.virtual_machines.names
}
output "virtual_machine_uuids" {
  value = module.virtual_machines.uuids
}
output "default_ip_addresses" {
  value = module.virtual_machines.default_ip_addresses
}