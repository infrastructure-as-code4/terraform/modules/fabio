locals {
  fabio_config_file = templatefile("${path.module}/files/fabio.conf.tpl",
  {
    proxy_port = var.loadbalancer_options.proxy_port
  })
  keepalived_config_file = templatefile("${path.module}/files/keepalived.conf.tpl",
  {
    state = ""
    priority = 0
    password = random_password.keepalived_password.result
    vip_address = var.loadbalancer_options.vip_address
    interface = "ens192"
  })
}


resource "null_resource" "fabio_configuration" {
  count = length(var.ssh_hosts)

  triggers = {
    fabio_options = jsonencode(local.fabio_config_file)
  }

  connection {
    host = var.ssh_hosts[count.index]
    user = var.ssh_credentials.username
    password = var.ssh_credentials.password
  }

  provisioner "file" {
    content = templatefile("${path.module}/files/fabio.conf.tpl",
    {
        proxy_port = var.loadbalancer_options.proxy_port
    })
    destination = "/tmp/fabio.conf"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo ufw allow ${var.loadbalancer_options.proxy_port}",
      "sudo mv /tmp/fabio* /etc/fabio.d/",
      "sudo systemctl restart fabio"
    ]
  }
}

resource "null_resource" "fabio_certificates" {
  count = length(var.ssh_hosts)

  depends_on = [
    null_resource.fabio_configuration
  ]

  connection {
    host = var.ssh_hosts[count.index]
    user = var.ssh_credentials.username
    password = var.ssh_credentials.password
  }

  provisioner "file" {
    content = var.tls_certificates.cert
    destination = "/tmp/fabio.crt"
  }
  provisioner "file" {
    content = var.tls_certificates.key
    destination = "/tmp/fabio.key"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/fabio* /etc/fabio.d/",
      "sudo systemctl restart fabio"
    ]
  }
}

resource "random_password" "keepalived_password" {
  length = 12
  special = false
}

resource "null_resource" "keepalived_master_configuration" {
  count = length(var.ssh_hosts)
  depends_on = [
    random_password.keepalived_password
  ]

  triggers = {
    keepalived_options = jsonencode(local.keepalived_config_file)
  }

  connection {
    host = var.ssh_hosts[count.index]
    user = var.ssh_credentials.username
    password = var.ssh_credentials.password
  }

  provisioner "file" {
    content = templatefile("${path.module}/files/keepalived.conf.tpl",
    {
      state = count.index == 0 ? "MASTER" : "BACKUP"
      priority = 255 - count.index
      password = random_password.keepalived_password.result
      vip_address = var.loadbalancer_options.vip_address
      interface = "ens192"
    })
    destination = "/tmp/keepalived.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/keepalived* /etc/keepalived/",
      "sudo systemctl restart keepalived"
    ]
  }
}
