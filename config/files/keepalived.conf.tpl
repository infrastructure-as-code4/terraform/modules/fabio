vrrp_instance VI_1 {
        state ${state}
        interface ${interface}
        virtual_router_id 51
        priority ${priority}
        advert_int 1
        authentication {
              auth_type PASS
              auth_pass ${password}
        }
        virtual_ipaddress {
              ${vip_address}/24
        }
}