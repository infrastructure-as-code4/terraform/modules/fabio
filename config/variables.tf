variable "ssh_hosts" {
  default = []
  type = list(string)
}

variable "ssh_credentials" {
  type = object({
    username = string
    password = string
  })
  sensitive = true
}

variable "loadbalancer_options" {
  type = object({
    proxy_port = number
    vip_address = string
  })
  default = {
    proxy_port = 9999
    vip_address = ""
  }
}
variable "tls_certificates" {
  type = object({
    cert = string
    key = string
  })
  sensitive = true
}